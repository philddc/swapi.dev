// Write you code here

import fetch from "node-fetch";

const filmsId = process.argv.slice(2)[0]; // get the first argument from the command line

const url = "https://swapi.dev/api";

function getFilm(filmId) {
  return fetch(`${url}/films/${filmId}`)
    .then((response) => response.json())
    .then((data) => data)
    .catch((error) => console.error(error));
}

function getPlanet(url) {
  return fetch(url)
    .then((response) => response.json())
    .then((data) => data)
    .catch((error) => console.error(error));
}

function main() {
  getFilm(filmsId)
    .then((film) => film.planets)
    .then((planetsUrl) => Promise.all(planetsUrl.map((url) => getPlanet(url))))
    .then((planets) => planets.filter((planet) => planet.surface_water > 0 && planet.terrain.includes("mountain")))
    .then((planetsMountainWater) => planetsMountainWater.map((planet) => Number(planet.diameter)))
    .then((diameters) => diameters.reduce((sum, diameter) => sum + diameter, 0))
    .then((sum) => console.log(sum))
    .catch((error) => console.error(error));
}

main();